var express = require('express');

var app = express();
var http = require('http');
var fs = require('fs');
var path = require('path');
var bodyParser = require('body-parser');
var multer = require('multer');
var upload = multer({dest: './tempFiles'});

var allowCrossDomain = function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');

    next();
};
app.use(allowCrossDomain);

app.use(function(req, res, next) {
    console.log('\nINFO LOGGED ON EACH REQUEST\nMethod : ' + req.method + '\nUrl : ' + req.url + '\nParameters : ' + req.params +'\n');
    next();
});
app.use(express.static(__dirname + '/app'));

app.post('/api/uploadFiles', upload.single('file'), function (req, res) {

    app.send
    console.log('FILE DATA')
    console.log(req.file);

    console.log('\nFILE CONTENTS AFTER READ');
    syncContent = fs.readFileSync(req.file.path);
        console.log(syncContent);

    console.log('JSON DATA RETURNED FROM LINGUAX');

    var base64Data = new Buffer(syncContent).toString('base64');

    var sendData = {
        action: 'parse',
        apiKey: 'MIIBuwIBAAKBgQCEoUZepajIhJd6ZFjCPqDGaUIicqMPPSWre0T2mlTqerQOcg',
        sender: 'startdate@hireground',
        response: 'plainTextJson',
        data: {
            content: base64Data,
            fileName: 'resume.txt',
            jobDesc: 'something'
        }
    };

    var options = {
        host: '192.168.1.217',
        path: '/',
        method: 'POST',
        port: 9000
    };

    var reqRes = http.request(options, function (response) {
        var str = '';

        response.on('data', function (chunk) {
            str += chunk;
        });

        response.on('end', function () {

            res.send

            res.data = str;
            console.log(res.data);
            res.end(str);
        });
    });

    reqRes.on('error', function (err) {
        console.log(err);
    });

    reqRes.write(JSON.stringify(sendData));

    reqRes.end();

});

app.listen(5000, function () {
    console.log('✔ Express server listening on port 5000....' +
        '\n\tThis is "fiddle Test"');
});