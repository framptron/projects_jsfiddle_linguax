'use strict';

angular.module('myApp.view1', ['ngRoute', 'ngFileUpload', 'ngAnimate', 'ngSanitize', 'ui.bootstrap'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/view1', {
            templateUrl: 'view1/view1.html',
            controller: 'View1Ctrl'
        });
    }])

    .controller('View1Ctrl', ['$scope', 'Upload', '$timeout', '$http', function ($scope, Upload, $timeout, $http) {



        $scope.uploadFiles = function (files, errFiles) {

            $scope.returnedDataArray = [];
            var arrayIncrement = 0;

            $scope.files = files;
            var fileLength = files.length;
            var fileCounter = 0;
            var percentComp = 0;
            if (files && files.length) {

                $scope.errFiles = errFiles;
                angular.forEach(files, function (file) {

                    file.upload = Upload.upload({
                        url: '/api/uploadFiles',
                        data: {file: file}
                    });
                    file.upload.then(function (response) {
                        $timeout(function () {

                            fileCounter++;
                            $scope.decimalPercent = (fileCounter / fileLength);
                            $scope.percentComp = ($scope.decimalPercent*100).toFixed(2);

                            console.log(percentComp);

                            file.result = response.data;
                            var testRawText = response.data.rawText;

                            $scope.returnedDataArray.push(response.data);
                            var returnedDataArray = new Array();
                            returnedDataArray.push(response.data);

                            console.log(file.result);

                            var resTrue = response.data;
                            $scope.resTrue = resTrue;

                            console.log(response.data.hgJson.Objective);
                            console.log(response.data.hgJson.SectionText.Experience);

                            var positions = response.data.hrJson.PositionHistory;

                            try {
                                for (var i = 0; i < positions.length; i++ ) {
                                    var sentences = positions[i]["Description"].split('.');
                                    sentences.pop(); // remove last empty element
                                    positions[i].sentences = sentences;
                                }
                            } catch (err) {
                                // alert("Error with splitting job description\n\nDetails:\n" + err);
                            }

                            $scope.returnedDataArray.positions = new Array();
                            $scope.returnedDataArray.positions.push(positions);

                        });
                    }, function (response) {
                        if (response.status > 0)
                            $scope.errorMsg = response.status + ': ' + response.data;
                    }, function (evt) {
                        file.progress = Math.min(100, parseInt(100.0 *
                            evt.loaded / evt.total));
                    });
                });
            } else {
                return false;
            }
        }; //upload

    }]).controller('AccordionCtrl', function ($scope) {
    $scope.oneAtATime = true;

    $scope.groups = [
        {
            title: 'Testing',
            content: 'more testing '
        },
        {
            title: 'Dynamic Group Header - 2',
            content: 'Dynamic Group Body - 2'
        }
    ];

    $scope.items = ['Item 1', 'Item 2', 'Item 3'];

    $scope.addItem = function () {
        var newItemNo = $scope.items.length + 1;
        $scope.items.push('Item ' + newItemNo);
    };

    $scope.status = {
        isCustomHeaderOpen: false,
        isFirstOpen: true,
        isFirstDisabled: false
    };

}).controller('checkBoxCtrl', function($scope) {
    // CHECK BOX ALL
    $scope.checkAll = function() {
        angular.forEach($scope.returnedDataArray, function (person) {
            person.Selected = $scope.selectAll;
        });
    };

})

;